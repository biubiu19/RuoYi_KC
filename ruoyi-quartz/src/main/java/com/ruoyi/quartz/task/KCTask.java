package com.ruoyi.quartz.task;

import com.ruoyi.system.domain.ZyjEqui;
import com.ruoyi.system.service.IZyjEquiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("kcTask")
public class KCTask {

    @Autowired
    private IZyjEquiService zyjEquiService;
    public void printEqui(){
        List<ZyjEqui> zyjEquis = zyjEquiService.selectZyjEquiList(new ZyjEqui());
        System.out.println(zyjEquis.toString());
    }
    public void printEquiById(Long id){
        System.out.println("id = " + id);
        ZyjEqui zyjEquis = zyjEquiService.selectZyjEquiById(id);
        System.out.println(zyjEquis.toString());
    }
}
