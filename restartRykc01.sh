#!/bin/bash
cd /opt/projects/RuoYi_KC/
git pull
mvn clean install
cd ruoyi-admin
docker build -t kc/kc01ry:1.0 .
docker stop kcweb
docker run -d -it --rm -p 85:80 --name kcweb kc/kc01ry:1.0

