package com.ruoyi.web.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CORSConfig {

    @Bean
    public WebMvcConfigurer configuration() {
        return new WebMvcConfigurer(){
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")// 设置允许跨域的路径(匹配所有路径)
                        .allowedHeaders("*")// 允许跨域请求的header(允许所有的请求头)
                        .allowedMethods("GET", "POST", "PUT", "DELETE")// 设置允许的方法
                        .allowedOrigins("*");// 设置允许跨域请求的域名(允许所有域名)
            }
        };
    }
}
