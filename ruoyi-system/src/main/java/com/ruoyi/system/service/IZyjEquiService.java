package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ZyjEqui;

/**
 * 器材管理Service接口
 * 
 * @author ruoyi
 * @date 2024-02-29
 */
public interface IZyjEquiService 
{
    /**
     * 查询器材管理
     * 
     * @param id 器材管理主键
     * @return 器材管理
     */
    public ZyjEqui selectZyjEquiById(Long id);

    /**
     * 查询器材管理列表
     * 
     * @param zyjEqui 器材管理
     * @return 器材管理集合
     */
    public List<ZyjEqui> selectZyjEquiList(ZyjEqui zyjEqui);

    /**
     * 新增器材管理
     * 
     * @param zyjEqui 器材管理
     * @return 结果
     */
    public int insertZyjEqui(ZyjEqui zyjEqui);

    /**
     * 修改器材管理
     * 
     * @param zyjEqui 器材管理
     * @return 结果
     */
    public int updateZyjEqui(ZyjEqui zyjEqui);

    /**
     * 批量删除器材管理
     * 
     * @param ids 需要删除的器材管理主键集合
     * @return 结果
     */
    public int deleteZyjEquiByIds(String ids);

    /**
     * 删除器材管理信息
     * 
     * @param id 器材管理主键
     * @return 结果
     */
    public int deleteZyjEquiById(Long id);

    int addOne(Integer id);
}
