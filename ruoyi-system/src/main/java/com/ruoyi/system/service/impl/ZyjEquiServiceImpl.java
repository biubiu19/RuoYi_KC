package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ZyjEquiMapper;
import com.ruoyi.system.domain.ZyjEqui;
import com.ruoyi.system.service.IZyjEquiService;
import com.ruoyi.common.core.text.Convert;

/**
 * 器材管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-02-29
 */
@Service
public class ZyjEquiServiceImpl implements IZyjEquiService 
{
    @Autowired
    private ZyjEquiMapper zyjEquiMapper;

    /**
     * 查询器材管理
     * 
     * @param id 器材管理主键
     * @return 器材管理
     */
    @Override
    public ZyjEqui selectZyjEquiById(Long id)
    {
        return zyjEquiMapper.selectZyjEquiById(id);
    }

    /**
     * 查询器材管理列表
     * 
     * @param zyjEqui 器材管理
     * @return 器材管理
     */
    @Override
    public List<ZyjEqui> selectZyjEquiList(ZyjEqui zyjEqui)
    {
        return zyjEquiMapper.selectZyjEquiList(zyjEqui);
    }

    /**
     * 新增器材管理
     * 
     * @param zyjEqui 器材管理
     * @return 结果
     */
    @Override
    public int insertZyjEqui(ZyjEqui zyjEqui)
    {
        return zyjEquiMapper.insertZyjEqui(zyjEqui);
    }

    /**
     * 修改器材管理
     * 
     * @param zyjEqui 器材管理
     * @return 结果
     */
    @Override
    public int updateZyjEqui(ZyjEqui zyjEqui)
    {
        return zyjEquiMapper.updateZyjEqui(zyjEqui);
    }

    /**
     * 批量删除器材管理
     * 
     * @param ids 需要删除的器材管理主键
     * @return 结果
     */
    @Override
    public int deleteZyjEquiByIds(String ids)
    {
        return zyjEquiMapper.deleteZyjEquiByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除器材管理信息
     * 
     * @param id 器材管理主键
     * @return 结果
     */
    @Override
    public int deleteZyjEquiById(Long id)
    {
        return zyjEquiMapper.deleteZyjEquiById(id);
    }

    @Override
    public int addOne(Integer id) {
        return zyjEquiMapper.addOne(id);
    }
}
