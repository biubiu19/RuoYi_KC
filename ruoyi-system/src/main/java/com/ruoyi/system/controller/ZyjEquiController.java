package com.ruoyi.system.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ZyjEqui;
import com.ruoyi.system.service.IZyjEquiService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 器材管理Controller
 * 
 * @author ruoyi
 * @date 2024-02-29
 */
@Controller
@RequestMapping("/system/equi")
public class ZyjEquiController extends BaseController
{
    private String prefix = "system/equi";

    @Autowired
    private IZyjEquiService zyjEquiService;

    @RequiresPermissions("system:equi:view")
    @GetMapping()
    public String equi()
    {
        return prefix + "/equi";
    }

    /**
     * 查询器材管理列表
     */
    @RequiresPermissions("system:equi:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ZyjEqui zyjEqui)
    {
        startPage();
        List<ZyjEqui> list = zyjEquiService.selectZyjEquiList(zyjEqui);
        return getDataTable(list);
    }

    /**
     * 导出器材管理列表
     */
    @RequiresPermissions("system:equi:export")
    @Log(title = "器材管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ZyjEqui zyjEqui)
    {
        List<ZyjEqui> list = zyjEquiService.selectZyjEquiList(zyjEqui);
        ExcelUtil<ZyjEqui> util = new ExcelUtil<ZyjEqui>(ZyjEqui.class);
        return util.exportExcel(list, "器材管理数据");
    }

    /**
     * 新增器材管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存器材管理
     */
    @RequiresPermissions("system:equi:add")
    @Log(title = "器材管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ZyjEqui zyjEqui)
    {
        return toAjax(zyjEquiService.insertZyjEqui(zyjEqui));
    }

    /**
     * 修改器材管理
     */
    @RequiresPermissions("system:equi:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ZyjEqui zyjEqui = zyjEquiService.selectZyjEquiById(id);
        mmap.put("zyjEqui", zyjEqui);
        return prefix + "/edit";
    }

    /**
     * 修改保存器材管理
     */
    @RequiresPermissions("system:equi:edit")
    @Log(title = "器材管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ZyjEqui zyjEqui)
    {
        return toAjax(zyjEquiService.updateZyjEqui(zyjEqui));
    }

    /**
     * 删除器材管理
     */
    @RequiresPermissions("system:equi:remove")
    @Log(title = "器材管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(zyjEquiService.deleteZyjEquiByIds(ids));
    }
    /**
     * 该器材数量+1
     */
    @RequiresPermissions("system:equi:addone")
    @Log(title = "器材管理", businessType = BusinessType.UPDATE)
    @GetMapping( "/addOne/{id}")
    @ResponseBody
    public AjaxResult addOne(@PathVariable("id") Integer id)
    {
        int count = zyjEquiService.addOne(id);
        String massage = "";
        if (count == 1){
            massage = "添加成功";
        }else {
            massage = "添加失败";
        }

        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("massage", massage);
        System.out.println("massage = " + massage);
        return ajaxResult;
    }



}
