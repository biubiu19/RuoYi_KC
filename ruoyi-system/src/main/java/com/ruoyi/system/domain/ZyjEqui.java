package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 器材管理对象 zyj_equi
 * 
 * @author ruoyi
 * @date 2024-02-29
 */
public class ZyjEqui extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 器材名 */
    @Excel(name = "器材名")
    private String equiName;

    /** 数量 */
    @Excel(name = "数量")
    private Long quantiyi;

    /** 单价 */
    @Excel(name = "单价")
    private Long price;

    /** 能否出借 */
    @Excel(name = "能否出借")
    private Integer lend;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setEquiName(String equiName) 
    {
        this.equiName = equiName;
    }

    public String getEquiName() 
    {
        return equiName;
    }
    public void setQuantiyi(Long quantiyi) 
    {
        this.quantiyi = quantiyi;
    }

    public Long getQuantiyi() 
    {
        return quantiyi;
    }
    public void setPrice(Long price) 
    {
        this.price = price;
    }

    public Long getPrice() 
    {
        return price;
    }
    public void setLend(Integer lend) 
    {
        this.lend = lend;
    }

    public Integer getLend() 
    {
        return lend;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("equiName", getEquiName())
            .append("quantiyi", getQuantiyi())
            .append("price", getPrice())
            .append("lend", getLend())
            .toString();
    }
}
